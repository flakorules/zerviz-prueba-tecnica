# Solución zerviz-prueba-tecnica

Solución al ejercicio propuesto de integración con api de Hacker News

## Diseño de la solución

![](assets/20220225_162005_components-diagram.png)

La solución implementada consta de un proyecto creado a partir del framework NestJS más una base de datos Mongo DB, el detalle de cada de las piezas es el siguiente:

#### HackerNewsDB

Base de datos creada en la nube MongoDB, donde la coleccion "hits" posee la siguiente estructura:

```
{
story_title:string,
title:string,
story_url:string,
url:string,
author:string,
created_at:string
}
```

#### HackerNewsModule

Este modulo declara el servicio HackerNewsService que se encarga de invocar la API de Hacker News.

#### HitsModule

Modulo que se encarga de manipular la base de datos a través del servicio HitsService. Adicionalmente, expone los siguientes endpoints en HitsController para acceder a la base de datos a través de http:

* GET http://{servidor}/hits
  Para obtener todos los registros desde la colección "hits" en base de datos HackerNewsDB.
* GET http://{servidor}/hits/{id}
  Para obtener un registro desde la colección "hits" en base de datos HackerNewsDB a partir de su id.
* DELETE http://{servidor}/hits/{id}
  Para eliminar un registro desde la colección "hits" en base de datos HackerNewsDB a partir de su id.
* POST http://{servidor}/integration
  Para llamar al módulo de integración que invoca la api de Hacker News e inserta en base de datos HackerNewsDB. Este endpoint se creó sólo para hacer más práctica la integración y así no esperar una hora por la ejecución del Cron.IntegrationModule

#### IntegrationModule

Módulo cuyo objetivo es realizar la integración entre la api de Hacker News y la base de datos HackerNewsDB. Para tal objetivo, el método integrate utiliza el decorador @Cron para que este mismo se ejecute cada una hora.

```javascript
@Cron(CronExpression.EVERY_HOUR)
  integrate() {
      /*...*/
  }
```

![](assets/20220225_170359_integration-sequence-diagram.png)

## Descargar código fuente

```bash
$ git clone https://github.com/flakorules/zerviz-prueba-tecnica.git
```

## Instalar dependencias del proyecto

```bash
$ npm install
```

## Variables de entorno del proyecto

Antes de ejecutar el ambiente de desarrollo, considerar crear archivo .env en directorio raiz del proyecto. Se asume que la base de datos debe estar creada en un ambiente local o remoto

```bash
HN_URL=http://hn.algolia.com/api/v1/search_by_date?query=nodejs
MONGO_SERVER=<url a la base de datos MongoDB>
```

## Ejecutar la aplicación en ambiente de desarrollo

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Ejecutar la aplicación en Docker Desktop

```bash
$ docker-compose up -d --build
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

#### Cobertura de código en statements

Se excluyen de la cobertura de código los archivos main.ts y *.module.ts


![](assets/20220227_153339_test-coverage.png)

## Links

- Website -[https://flakorules.github.io/portfolio](https://flakorules.github.io/portfolio)
- LinkedIn -[https://www.linkedin.com/in/cristian-cisternas/](https://www.linkedin.com/in/cristian-cisternas/)
- Github -[https://github.com/flakorules](https://github.com/flakorules)
