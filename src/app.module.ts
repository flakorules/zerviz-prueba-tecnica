import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNewsModule } from './hacker-news/hacker-news.module';
import { HitsModule } from './hits/hits.module';
import { IntegrationModule } from './integration/integration.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    HackerNewsModule,
    HitsModule,
    MongooseModule.forRoot(process.env.MONGO_SERVER),
    IntegrationModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
