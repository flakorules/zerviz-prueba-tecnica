import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HackerNewsService } from '../hacker-news/hacker-news.service';
import { HitDto } from '../shared/hit.dto';
import { HitsService } from '../hits/hits.service';

@Injectable()
export class IntegrationService {
  constructor(
    private readonly hackerNewsService: HackerNewsService,
    private readonly hitService: HitsService,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  integrate() {
    this.hackerNewsService.findAll().subscribe(async ({ data }) => {
      const dataToInsert: HitDto[] = data.hits.map(
        (hit) =>
          new HitDto(
            hit.story_title,
            hit.title,
            hit.story_url,
            hit.url,
            hit.author,
            hit.created_at,
          ),
      );

      const insertedData = await this.hitService.insertAll(dataToInsert);
      console.log(
        `${insertedData.length} rows has been inserted at ${new Date()}`,
      );
      return dataToInsert;
    });
  }
}
