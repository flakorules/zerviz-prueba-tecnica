import { Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { IntegrationService } from './integration.service';

@Controller('integration')
export class IntegrationController {
  constructor(private readonly integrationService: IntegrationService) {}

  @HttpCode(HttpStatus.OK)
  @Post()
  post() {
    this.integrationService.integrate();
    return 'Integration succeed';
  }
}
