import { Module } from '@nestjs/common';
import { IntegrationService } from './integration.service';
import { HackerNewsModule } from '../hacker-news/hacker-news.module';
import { ScheduleModule } from '@nestjs/schedule';
import { HitsModule } from '../hits/hits.module';
import { IntegrationController } from './integration.controller';

@Module({
  providers: [IntegrationService],
  imports: [HackerNewsModule, HitsModule, ScheduleModule.forRoot()],
  controllers: [IntegrationController],
})
export class IntegrationModule {}
