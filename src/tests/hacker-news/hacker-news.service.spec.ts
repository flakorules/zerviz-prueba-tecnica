import { Test, TestingModule } from '@nestjs/testing';
import { HackerNewsService } from '../../hacker-news/hacker-news.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

describe('HackerNewsService', () => {
  let service: HackerNewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HackerNewsService],
      imports: [HttpModule, ConfigModule.forRoot({ isGlobal: true })],
    }).compile();

    service = module.get<HackerNewsService>(HackerNewsService);
  });

  it('HackerNewsService - should be defined', () => {
    expect(service).toBeDefined();
  });

  it('HackerNewsService - should call find all method', (done) => {
    service.findAll().subscribe((response) => {
      const actual: number = response.data.hits.length;
      expect(actual).toBeGreaterThan(0);
      done();
    });
  });
});
