import { HitDto } from '../../shared/hit.dto';
describe('Hit Dto', () => {
  it('Should create an HitDto object', () => {
    const obj: HitDto = new HitDto(
      'story_title',
      'title',
      'story_url',
      'url',
      'author',
      new Date(),
    );
    expect(obj).not.toBeNull();
  });
});
