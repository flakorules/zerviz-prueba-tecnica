import { Test, TestingModule } from '@nestjs/testing';
import { HitsService } from '../../hits/hits.service';
import { HitsController } from '../../hits/hits.controller';
import { hitMock, hitsMock } from '../mocks/hits.mocks';

describe('HitsController', () => {
  let hitsController: HitsController;
  let hitsService: HitsService;

  beforeAll(async () => {
    const hitsServiceProvider = {
      provide: HitsService,
      useFactory: () => ({
        findAll: jest.fn(() => hitsMock),
        findOne: jest.fn(() => hitMock),
        delete: jest.fn(() => hitMock),
      }),
    };

    const app: TestingModule = await Test.createTestingModule({
      controllers: [HitsController],
      providers: [hitsServiceProvider],
    }).compile();

    hitsController = app.get<HitsController>(HitsController);
    hitsService = app.get<HitsService>(HitsService);
  });

  it('HitsController - shoudl call findAll method', async () => {
    const actual = await hitsController.findAll();
    expect(hitsService.findAll).toHaveBeenCalled();
    expect(actual.length).toBeGreaterThan(0);
  });

  it('HitsController - should call findOne method', async () => {
    const actual = await hitsController.findOne('xyz');
    expect(hitsService.findOne).toHaveBeenCalled();
    expect(actual).toBe(hitMock);
  });

  it('HitsController - should call delete method', async () => {
    const actual = await hitsController.delete('xyz');
    expect(hitsService.delete).toHaveBeenCalled();
    expect(actual).toBe(hitMock);
  });
});
