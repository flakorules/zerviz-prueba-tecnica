import { HitsService } from '../../hits/hits.service';
import { HitDocument } from '../../hits/hits.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { hitMock, hitsMock } from '../mocks/hits.mocks';
describe('HitsService', () => {
  let hitsService: HitsService;
  let model: Model<HitDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HitsService,
        {
          provide: getModelToken('Hit'),

          useValue: {
            find: jest.fn(),
            findById: jest.fn(),
            findByIdAndDelete: jest.fn(),
            insertMany: jest.fn(() => hitsMock),
          },
        },
      ],
    }).compile();

    hitsService = module.get<HitsService>(HitsService);
    model = module.get<Model<HitDocument>>(getModelToken('Hit'));
  });

  it('HitsService - should be defined', () => {
    expect(hitsService).toBeDefined();
  });

  it('HitsService - should return all hits', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(hitsMock),
    } as any);
    const actual = await hitsService.findAll();
    expect(actual.length).toBeGreaterThan(0);
  });

  it('HitsService - should return one hit', async () => {
    jest.spyOn(model, 'findById').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(hitMock),
    } as any);
    const actual = await hitsService.findOne('xyz');
    expect(actual).toEqual(hitMock);
  });

  it('HitsService - should delete one hit', async () => {
    jest.spyOn(model, 'findByIdAndDelete').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(hitMock),
    } as any);
    const actual = await hitsService.delete('xyz');
    expect(actual).toEqual(hitMock);
  });

  it('HitsService - should insert a hit list', async () => {
    const actual = await hitsService.insertAll(hitsMock);
    expect(actual.length).toBeGreaterThan(0);
  });
});
