import { Test, TestingModule } from '@nestjs/testing';
import { IntegrationController } from '../../integration/integration.controller';
import { IntegrationService } from '../../integration/integration.service';
describe('IntegrationController', () => {
  let integrationController: IntegrationController;
  let integrationService: IntegrationService;

  beforeAll(async () => {
    const integrationServiceProvider = {
      provide: IntegrationService,
      useFactory: () => ({
        integrate: jest.fn(),
      }),
    };

    const app: TestingModule = await Test.createTestingModule({
      controllers: [IntegrationController],
      providers: [integrationServiceProvider],
    }).compile();

    integrationController = app.get<IntegrationController>(
      IntegrationController,
    );
    integrationService = app.get<IntegrationService>(IntegrationService);
  });

  it('HitsController - shoudl call post method', async () => {
    const actual = integrationController.post();
    const expected = 'Integration succeed';
    expect(integrationService.integrate).toHaveBeenCalled();
    expect(actual).toBe(expected);
  });
});
