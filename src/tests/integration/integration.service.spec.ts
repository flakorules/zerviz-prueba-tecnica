import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { HackerNewsService } from '../../hacker-news/hacker-news.service';
import { HitsService } from '../../hits/hits.service';
import { IntegrationService } from '../../integration/integration.service';
import { hitsMock } from '../mocks/hits.mocks';

describe('IntegrationService', () => {
  let hackerNewsService: HackerNewsService;
  let hitsService: HitsService;
  let integrationService: IntegrationService;

  beforeAll(async () => {
    const hackerNewsServiceProvider = {
      provide: HackerNewsService,
      useFactory: () => ({
        findAll: jest.fn(() =>
          of({
            data: {
              hits: hitsMock,
            },
          }),
        ),
      }),
    };

    const hitsServiceProvider = {
      provide: HitsService,
      useFactory: () => ({
        insertAll: jest.fn(() => hitsMock),
      }),
    };

    const app: TestingModule = await Test.createTestingModule({
      providers: [
        hitsServiceProvider,
        hackerNewsServiceProvider,
        IntegrationService,
      ],
    }).compile();

    hackerNewsService = app.get<HackerNewsService>(HackerNewsService);
    hitsService = app.get<HitsService>(HitsService);
    integrationService = app.get<IntegrationService>(IntegrationService);
  });

  it('IntegrationService - should call integrate method', () => {
    integrationService.integrate();
    expect(hackerNewsService.findAll).toHaveBeenCalled();
    expect(hitsService.insertAll).toHaveBeenCalled();
  });
});
