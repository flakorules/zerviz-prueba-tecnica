export const hitMock = {
  story_title: 'story_title',
  title: 'title',
  story_url: 'story_url',
  url: 'url',
  author: 'author',
  created_at: new Date(),
};

export const hitsMock = [
  {
    ...hitMock,
  },
];
