import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HitDocument = Hit & Document;

@Schema()
export class Hit {
  @Prop()
  story_title?: string;
  @Prop()
  title?: string;
  @Prop()
  story_url?: string;
  @Prop()
  url?: string;
  @Prop()
  author?: string;
  @Prop()
  created_at?: Date;
}

export const HitSchema = SchemaFactory.createForClass(Hit);
