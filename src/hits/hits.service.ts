import { Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Hit, HitDocument } from './hits.schema';
import { HitDto } from 'src/shared/hit.dto';

@Injectable()
export class HitsService {
  constructor(
    @InjectModel(Hit.name) private readonly model: Model<HitDocument>,
  ) {}

  async findAll(): Promise<Hit[]> {
    return await this.model.find().exec();
  }

  async findOne(id: string): Promise<Hit> {
    return await this.model.findById(id).exec();
  }

  async delete(id: string): Promise<Hit> {
    return await this.model.findByIdAndDelete(id).exec();
  }

  async insertAll(hits: HitDto[]) {
    return await this.model.insertMany(hits);
  }
}
