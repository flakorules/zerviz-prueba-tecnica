import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Res,
} from '@nestjs/common';
import { HitsService } from './hits.service';

@Controller('hits')
export class HitsController {
  constructor(private readonly hitsService: HitsService) {}

  @HttpCode(HttpStatus.OK)
  @Get()
  async findAll() {
    return await this.hitsService.findAll();
  }

  @HttpCode(HttpStatus.OK)
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.hitsService.findOne(id);
  }

  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.hitsService.delete(id);
  }
}
