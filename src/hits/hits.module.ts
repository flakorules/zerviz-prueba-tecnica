import { Module } from '@nestjs/common';
import { HitsService } from './hits.service';
import { HitsController } from './hits.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Hit, HitSchema } from './hits.schema';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [HitsController],
  providers: [HitsService, HttpModule],
  imports: [MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }])],
  exports: [HitsService],
})
export class HitsModule {}
