import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { HackerNewsResponse } from './hacker-news.interface';

@Injectable()
export class HackerNewsService {
  private readonly logger = new Logger(HackerNewsService.name);
  constructor(private readonly httpService: HttpService) {}

  findAll(): Observable<AxiosResponse<HackerNewsResponse>> {
    return this.httpService.get(process.env.HN_URL);
  }
}
