export interface HackerNewsResponse {
  hits: HitResponse[];
}

export interface HitResponse {
  story_title: null | string;
  title: null | string;
  story_url: null | string;
  url: null | string;
  author: string;
  created_at: Date;
}
