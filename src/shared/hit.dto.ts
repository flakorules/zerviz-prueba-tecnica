export class HitDto {
  constructor(
    story_title?: string,
    title?: string,
    story_url?: string,
    url?: string,
    author?: string,
    created_at?: Date,
  ) {
    this.story_title = story_title;
    this.title = title;
    this.story_url = story_url;
    this.url = url;
    this.author = author;
    this.created_at = created_at;
  }

  story_title?: string;
  title?: string;
  story_url?: string;
  url?: string;
  author?: string;
  created_at?: Date;
}
